package com.promolta.email.dao;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
@Entity
@Table(name = "message")
public class Message {

    private Long id;
    private User sender;
    private List<User> recipients;
    private String content;
    private Email email;
    private Date date;
    private boolean draft;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @OneToOne(targetEntity = User.class)
    public User getSender() {
        return sender;
    }

    public Message setSender(User sender) {
        this.sender = sender;
        return this;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    public List<User> getRecipients() {
        return recipients;
    }

    public Message setRecipients(List<User> recipients) {
        this.recipients = recipients;
        return this;
    }

    public String getContent() {
        return content;
    }

    public Message setContent(String content) {
        this.content = content;
        return this;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public Email getEmail() {
        return email;
    }

    public Message setEmail(Email email) {
        this.email = email;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public Message setDate(Date date) {
        this.date = date;
        return this;
    }

    public boolean isDraft() {
        return draft;
    }

    public Message setDraft(boolean draft) {
        this.draft = draft;
        return this;
    }
}
