package com.promolta.email.dao;

import javax.persistence.*;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
@Entity
@Table(name = "email")
public class Email {

  private int id;
  private String subject;
  private Message primaryMessage;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public int getId() {
    return id;
  }

  public Email setId(int id) {
    this.id = id;
    return this;
  }

  public String getSubject() {
    return subject;
  }

  public Email setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  @OneToOne(mappedBy = "email")
  public Message getPrimaryMessage() {
    return primaryMessage;
  }

  public Email setPrimaryMessage(Message primaryMessage) {
    this.primaryMessage = primaryMessage;
    return this;
  }
}
