package com.promolta.email.dao;

import javax.persistence.*;
import java.util.List;

/**
 * @author asishmohapatra
 * @since 30/12/18
 */
@Entity
@Table(name = "message_thread")
public class MessageThread {
  private Long id;
  private Email email;
  private List<Message> messages;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return id;
  }

  public MessageThread setId(Long id) {
    this.id = id;
    return this;
  }

  @OneToOne(targetEntity = Email.class)
  public Email getEmail() {
    return email;
  }

  public MessageThread setEmail(Email email) {
    this.email = email;
    return this;
  }

  @OneToMany(targetEntity = Message.class)
  public List<Message> getMessages() {
    return messages;
  }

  public MessageThread setMessages(List<Message> messages) {
    this.messages = messages;
    return this;
  }
}
