package com.promolta.email.service;

import com.promolta.email.dao.Email;
import com.promolta.email.dao.Message;
import com.promolta.email.repository.EmailRepository;
import com.promolta.email.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private EmailRepository emailRepository;

    public List<Email> findAllEmailsByUser(Long userId){
        return messageRepository.findEmailsByUser(userId);
    }

    public List<Message> findMessagesByEmail(Long id){
        return messageRepository.findAllByEmail_id(id);
    }
}
