package com.promolta.email.service;

import com.promolta.email.dao.*;
import com.promolta.email.dto.EmailDto;
import com.promolta.email.dto.EmailRequest;
import com.promolta.email.repository.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
@Service
public class EmailService {
  private final UserRepository userRepository;
  private final MessageRepository messageRepository;
  private final EmailRepository emailRepository;
  private final MessageThreadRepository messageThreadRepository;
  private final EmailStatusRepository emailStatusRepository;


  @Autowired
  public EmailService(UserRepository userRepository, MessageRepository messageRepository,
                      EmailRepository emailRepository, MessageThreadRepository messageThreadRepository,
                      EmailStatusRepository emailStatusRepository) {
    this.userRepository = userRepository;
    this.messageRepository = messageRepository;
    this.emailRepository = emailRepository;
    this.messageThreadRepository = messageThreadRepository;
    this.emailStatusRepository = emailStatusRepository;
  }

  /**
   * Retrieves inbox mails for user
   *
   * @param userId
   * @return
   */
  public List<Email> getEmails(Long userId) {
    return messageRepository.findEmailsByUser(userId);
  }

  /**
   * Retrieves All drafted emails for user
   *
   * @param userId
   * @return
   */
  public List<Email> getDraftedEmails(Long userId) {
    return messageRepository.findDraftedEmail(userId);
  }

  public List<Email> getSentEmails(Long userId) {
    return new ArrayList<>();
  }

  /**
   * Retrieves All thrashed emails for user
   *
   * @param userId
   * @return
   */
  public List<Email> getTrashedEmails(Long userId) {
    return messageRepository.findTrashedEmail(userId);
  }

  public EmailDto sendEmail(EmailRequest emailRequest, Long userId) {
    return new EmailDto(sendOrDraftEmail(emailRequest, userId, false));
  }

  public Email createDraft(EmailRequest emailRequest, Long userId) {
    return sendOrDraftEmail(emailRequest, userId, true);
  }

  public Email forwardEmail(EmailRequest emailRequest, Long userId) {
    User user = userRepository.findById(userId).get();
    List<User> recipients = userRepository.findByEmailIn(emailRequest.getEmailAddress());
    Email forwardEmail = emailRepository.findById(emailRequest.getEmailId()).get();
    Message message = forwardEmail.getPrimaryMessage();
    message.setRecipients(recipients);
    message.setSender(user);
    messageRepository.save(message);
    forwardEmail.setPrimaryMessage(message);
    emailRepository.save(forwardEmail);
    List<Message>  messages = new ArrayList<>();
    messages.add(message);
    MessageThread messageThread = new MessageThread()
      .setEmail(forwardEmail)
      .setMessages(messages);
    messageThreadRepository.save(messageThread);
    return forwardEmail;
  }

  private Email sendOrDraftEmail(EmailRequest emailRequest, Long userId, boolean draft) {
    User user = userRepository.findById(userId).get();
    List<User> recipients = userRepository.findByEmailIn(emailRequest.getEmailAddress());
    Email email = new Email();
    email.setSubject(emailRequest.getSubject());
    emailRepository.save(email);
    Message message = new Message()
      .setRecipients(recipients)
      .setSender(user)
      .setContent(emailRequest.getContent())
      .setEmail(email)
      .setDraft(draft).setDate(Date.valueOf(LocalDate.now()));
    messageRepository.save(message);
    List<EmailStatus> emailStatuses = new ArrayList<>();
    for(User user1 : recipients) {
      emailStatuses.add(new EmailStatus().setReadStatus(false).setEmail(email).setDeleted(false).setUser(user1));
    }
    emailStatusRepository.saveAll(emailStatuses);
//    emailRepository.save(email);
//    List<Message>  messages = new ArrayList<>();
//    messages.add(message);
//    MessageThread messageThread = new MessageThread()
//      .setEmail(email)
//      .setMessages(messages);
//    messageThreadRepository.save(messageThread);
    return email;
  }
}
