package com.promolta.email.repository;

import com.promolta.email.dao.Email;
import com.promolta.email.dao.Message;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {

    public List<Message> findAllByEmail_id(Long id);

    @Query("select e.email from Message as e inner join EmailStatus as st on e.id = st.email.id where :userId in (e.recipients) and st.user.id=:userId and st.deleted =0")
    public List<Email> findEmailsByUser(Long userId);

    @Query("select e.email from Message as e inner join EmailStatus as st on e.id = st.email.id where st.user.id=:userId and st.deleted =1")
    public List<Email> findThrashedEmail(Long userId);

    @Query("select e.email from Message as e inner join EmailStatus as st on e.id = st.email.id where st.user.id=:userId and e.draft= true and st.deleted =0")
    public List<Email> findDraftedEmail(Long userId);
}
