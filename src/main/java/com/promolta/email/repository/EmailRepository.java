package com.promolta.email.repository;

import com.promolta.email.dao.Email;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailRepository extends JpaRepository<Email, Long> {
}
