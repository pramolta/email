package com.promolta.email.repository;

import com.promolta.email.dao.MessageThread;
import com.promolta.email.dao.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageThreadRepository extends JpaRepository<MessageThread, Long> {
}