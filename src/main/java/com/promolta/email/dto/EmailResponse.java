package com.promolta.email.dto;

import com.promolta.email.dto.type.EmailSentStatus;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
public class EmailResponse {
  private String emailAddress;
  private EmailSentStatus emailSentStatus;

  public String getEmailAddress() {
    return emailAddress;
  }

  public EmailResponse setEmailAddress(String emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

  public EmailSentStatus getEmailSentStatus() {
    return emailSentStatus;
  }

  public EmailResponse setEmailSentStatus(EmailSentStatus emailSentStatus) {
    this.emailSentStatus = emailSentStatus;
    return this;
  }
}
