package com.promolta.email.dto;

import java.util.List;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
public class EmailRequest {
  private String content;
  private List<String> emailAddress;
  private String subject;
  private Long emailId;

  public String getContent() {
    return content;
  }

  public EmailRequest setContent(String content) {
    this.content = content;
    return this;
  }

  public List<String> getEmailAddress() {
    return emailAddress;
  }

  public EmailRequest setEmailAddress(List<String> emailAddress) {
    this.emailAddress = emailAddress;
    return this;
  }

  public String getSubject() {
    return subject;
  }

  public EmailRequest setSubject(String subject) {
    this.subject = subject;
    return this;
  }

  public Long getEmailId() {
    return emailId;
  }

  public EmailRequest setEmailId(Long emailId) {
    this.emailId = emailId;
    return this;
  }
}
