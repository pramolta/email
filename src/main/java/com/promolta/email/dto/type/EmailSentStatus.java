package com.promolta.email.dto.type;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
public enum EmailSentStatus {
  DELIVERED, FAILED;
}
