package com.promolta.email.controller;

import com.promolta.email.dao.Email;
import com.promolta.email.dao.Message;
import com.promolta.email.dto.EmailRequest;
import com.promolta.email.dto.EmailResponse;
import com.promolta.email.service.EmailService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * @author asishmohapatra
 * @since 29/12/18
 */
@RestController
public class EmailController {

  private final EmailService emailService;

  @Autowired
  public EmailController(final EmailService emailService) {
    this.emailService = emailService;
  }

  @RequestMapping(value = "/getEmails")
  public List<Email> getEmails(@RequestParam(value = "userId") Long userId) {
    return emailService.getEmails(userId);
  }

  @RequestMapping(value = "/getDraftedEmails")
  public List<Email> getDraftedEmails(@RequestParam(value = "userId") Long userId) {
    return emailService.getDraftedEmails(userId);
  }

  @RequestMapping(value = "/getSentEmails")
  public List<Email> getSentEmails(@RequestParam(value = "userId") Long userId) {
    return emailService.getSentEmails(userId);
  }

  @RequestMapping(value = "/getThrashed")
  public List<Email> getThrashedEmails(@RequestParam(value = "userId") Long userId) {
    return emailService.getThrashedEmails(userId);
  }

  @RequestMapping(value = "/sendEmail")
  public Email sendEmail(@RequestBody EmailRequest emailRequest, @RequestParam(value = "userId") Long userId) {
    return emailService.sendEmail(emailRequest, userId);
  }

  @RequestMapping(value = "/draftEmail")
  public Email draftEmail(@RequestBody EmailRequest emailRequest, @RequestParam(value = "userId") Long userId) {
    return emailService.createDraft(emailRequest, userId);
  }

  @RequestMapping(value = "/forwardEmail")
  public Email forwardEmail(@RequestBody EmailRequest emailRequest, @RequestParam(value = "userId") Long userId) {
    return emailService.forwardEmail(emailRequest, userId);
  }
}
